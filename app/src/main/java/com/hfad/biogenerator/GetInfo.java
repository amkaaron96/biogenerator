package com.hfad.biogenerator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class GetInfo extends Activity {

    public final static String MESSAGE_KEY = "finalBio.senddata.message_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_info);
    }

    public void onClickGenerateBio(View view) {
        String sendInfo = buildBio();
        Intent next = new Intent(GetInfo.this, ShowInfo.class);
        next.putExtra(MESSAGE_KEY, sendInfo);
        startActivity(next);
    }

    public String buildBio(){
        EditText fName = findViewById(R.id.firstName);
        String fname = fName.getText().toString();

        EditText lName = findViewById(R.id.lastName);
        String lname = lName.getText().toString();

        EditText sch = findViewById(R.id.school);
        String school = sch.getText().toString();

        EditText grad = findViewById(R.id.gradYear);
        String gradYear = grad.getText().toString();

        Spinner deg = findViewById(R.id.degree);
        String degree = deg.getSelectedItem().toString();

        Spinner maj = findViewById(R.id.major);
        String major = maj.getSelectedItem().toString();

        EditText favActs = findViewById(R.id.favActivities);
        String favActivities = favActs.getText().toString();

        String finalBio = fname + " " + lname + " " + "graduated in " + gradYear + " with a " + degree + " with a concentration in " + major + " from " + school + ". Their favorite activities are " + favActivities + ".";

        return finalBio;
    }
}
